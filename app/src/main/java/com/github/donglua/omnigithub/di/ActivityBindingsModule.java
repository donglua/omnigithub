package com.github.donglua.omnigithub.di;

import com.github.donglua.omnigithub.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class ActivityBindingsModule {

    @ContributesAndroidInjector
    abstract MainActivity mainActivityInjector();

}
