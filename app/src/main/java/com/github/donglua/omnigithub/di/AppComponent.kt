package com.github.donglua.omnigithub.di

import android.app.Application
import android.content.Context

import com.github.donglua.omnigithub.OmniGithubApp
import dagger.Binds

import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    ActivityBindingsModule::class
])
@Singleton
interface AppComponent : AndroidInjector<OmniGithubApp> {

    override fun inject(instance: OmniGithubApp)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): AppComponent.Builder

        fun build(): AppComponent
    }

}

@Module
abstract class AppModule {

    @Binds
    abstract fun bindContext(application: Application): Context
}
