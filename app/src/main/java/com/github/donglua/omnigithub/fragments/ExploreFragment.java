package com.github.donglua.omnigithub.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

public class ExploreFragment extends Fragment {

    public static ExploreFragment newInstance() {

        Bundle args = new Bundle();
        ExploreFragment fragment = new ExploreFragment();
        fragment.setArguments(args);
        return fragment;
    }

}
